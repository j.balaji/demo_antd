import React from "react";
import { Route, BrowserRouter, Switch , Redirect} from "react-router-dom";

import Home from "./pages/Home";

const Routes = () => {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route exact path="/home" component={Home} />
          <Redirect from="/" to="/home" />
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default Routes;
